<?php
/**
 * @file 
 * RSVP Module Hooks
 */
use Drupal\Core\Form\FormStateInterface;

/**
 * Alter the node add/edit form to include admin setting for displaying RSVPBlock with content
 */
function rsvplist_form_node_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id)
{
    $node = $form_state->getFormObject()->getEntity();
    $current_node_type = $node->getType();
    $config = \Drupal::config('rsvplist.settings');
    $types = $config->get('allowed_types', array());
    \Drupal::messenger()->addMessage($types);

    if(in_array($current_node_type, $types)){
        $form['rsvplist'] = array(
            '#type' => 'details',
            '#title' => t('RSVP Collection'),
            '#access' => \Drupal::currentUser()->hasPermission('administer rsvplist'),
            '#group' => 'advanced',
            '#weight' => 100
        );
        $enabler = \Drupal::service('rsvplist.enabler');
        $form['rsvplist']['rsvplist_enabled'] = array(
            '#type' => 'checkbox',
            '#title' => t('Collect RSVP e-mail addresses for this node'), 
            '#default_value' => $enabler->isEnabled($node)
        );
        
        foreach(array_keys($form['actions']) as $action){
            if($action != 'preview' && isset($form['actions'][$action]['#type']) && 
            $form['actions'][$action]['#type'] === 'submit') {
                $form['actions'][$action]['#submit'][] = 'rsvplist_form_node_form_submit';
            }
        }
    }
}

/**
 * Form submission handler for RSVP item field on the node form.
 *
 * @param array $form
 * @param FormStateInterface $form_state
 * @return void
 */
function rsvplist_form_node_form_submit(array $form, FormStateInterface $form_state)
{
    $enabler = \Drupal::service('rsvplist.enabler');
    $node = $form_state->getFormObject()->getEntity();
    if($enabled = $form_state->getValue('rsvplist_enabled')) {
        $enabler->setEnabled($node);
    }
    else {
        $enabler->delEnabled($node);
    }
}